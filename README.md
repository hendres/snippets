# linux

```sh
# ressource busy
# find process id, which uses mount (e.g. which process uses docker overlay resource)
grep docker /proc/*/mountinfo

# should show s.th. like that
# /proc/12345/mountinfo:159 149 0:36 /var/lib/docker/overlay/...

# check process id
ps -p 12345 -o comm=

# will respond with e.g.
# freshclam 

# crontab - write error and out to current time log
0 23 * * * root dosth.sh &> /var/log/osot-`date "+\%Y-\%m-\%d-\%H:\%M:\%S"`.log

# Journalctl
# see https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs#journal-filtering-by-time
journalctl --since "2015-01-10" --until "2015-01-11 03:00"
journalctl --since yesterday
journalctl --since 09:00 --until "1 hour ago"
journalctl -u nginx.service -u php-fpm.service --since today
journalctl _UID=33 --since today
journalctl -p err -b

# ACL
# show ACL
getfacl <path-to-directory>

# set default acl (e.g. to inherit access rights from parent folder)
setfacl -d -m group::rwx <path-to-parent-folder>

# grep and show only 30 characters left and right the found pattern
grep -Po '.{0,30}pattern.{0,30}' *.js

```

# openshift/kubernetes

```sh
# run image based container
oc run -i --tty --image busybox sample --restart=Never --rm /bin/sh

# show all rolebindings per project
oc get rolebindings -n project

# add serviceaccount to role view
oc policy add-role-to-user view -z serviceaccount

# remove serviceaccount from role view
oc policy remove-role-from-user view -z serviceaccount

```

# git

```sh
# reset branch on remote repo
git reset --hard <commit-hash>
git push -f origin master

# resets current branch to <commit> without any changes to working dir
git reset <commit>

# resets current branch to <commit> incl. cleaning working dir
git reset --hard <commit>

# undo changes with a new commit
git revert HEAD

# master has changed, so you have rebase your branch to the new HEAD of master
git pull --rebase origin master

# if there were merge conflicts, resolve and add the changes
got add <resolved-merge-conflict-files>

# than continue rebase and force push
git rebase --continue
git push --force origin <actual_branch>
```

# ansible

```sh
# ad-hoc
ansible localhost -m lineinfile -a "dest=openshift-ansible/inventory/hosts regexp=openshift_logging_install_logging=false line=openshift_logging_install_logging=true" -c local
```